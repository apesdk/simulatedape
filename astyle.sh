astyle --style=allman apesdk/longterm.c

astyle --style=allman apesdk/entity/*.c
astyle --style=allman apesdk/entity/*.h

astyle --style=allman apesdk/external/png/*.c
astyle --style=allman apesdk/external/png/*.h

./astyle --style=allman apesdk/gui/*.c
./astyle --style=allman apesdk/gui/*.h

./astyle --style=allman apesdk/universe/*.c
./astyle --style=allman apesdk/universe/*.h

./astyle --style=allman apesdk/entity/*.c
./astyle --style=allman apesdk/entity/*.h

./astyle --style=allman apesdk/test/*.c
./astyle --style=allman apesdk/toolkit/test/*.c

./astyle --style=allman apesdk/universe/*.c
./astyle --style=allman apesdk/universe/*.h

./astyle --style=allman apesdk/win/*.c
./astyle --style=allman apesdk/win/*.h

./astyle --style=allman apesdk/toolkit/*.c
./astyle --style=allman apesdk/toolkit/*.h
