﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ApeSimulationSubsystem.h"

#include "ApeSDK.h"
#include "ApeSDKTypes.h"

void UApeSimulationSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);

	ApeSDKWrapper = FApeSDKModule::Get().GetApeSDKWrapper();

	const FRandomStream RandomStream(FDateTime::UtcNow().GetTicks());

	ApeSDKWrapper.Pin()->SimInit(EKindOfUse::KIND_START_UP, RandomStream.GetUnsignedInt(), MAP_AREA, 0);
}

void UApeSimulationSubsystem::Deinitialize()
{
	Super::Deinitialize();

	ApeSDKWrapper.Pin()->SimClose();
}

FString UApeSimulationSubsystem::GetOutputString() const
{
	return ApeSDKWrapper.Pin()->SimOutputString();
}

void UApeSimulationSubsystem::Tick(float Delta)
{
	Super::Tick(Delta);
	ApeSDKWrapper.Pin()->SimCycle();
}

TStatId UApeSimulationSubsystem::GetStatId() const
{
	RETURN_QUICK_DECLARE_CYCLE_STAT(UApeSimulationSubsystem, STATGROUP_Tickables);
}