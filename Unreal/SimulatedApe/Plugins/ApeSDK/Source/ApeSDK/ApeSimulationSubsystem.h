﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "ApeSimulationSubsystem.generated.h"

struct FApeSDKDLLWrapper;

/**
 * 
 */
UCLASS()
class APESDK_API UApeSimulationSubsystem : public UTickableWorldSubsystem
{
	GENERATED_BODY()

public:

	virtual void Initialize(FSubsystemCollectionBase& Collection) override;

	virtual void Deinitialize() override;

	virtual void Tick(float Delta) override;

	virtual TStatId GetStatId() const override;

	UFUNCTION(BlueprintPure)
	FString GetOutputString() const;

protected:

	TWeakPtr<FApeSDKDLLWrapper> ApeSDKWrapper;
};
