﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ApeSimulationMainWidget.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class APESDK_API UApeSimulationMainWidget : public UUserWidget
{
	GENERATED_BODY()
};
