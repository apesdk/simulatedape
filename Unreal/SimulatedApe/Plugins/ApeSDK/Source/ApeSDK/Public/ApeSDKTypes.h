﻿#pragma once



#define MAP_BITS						(9)
#define MAP_DIMENSION					(1<<(MAP_BITS))
#define MAP_AREA                      (1<<(2*MAP_BITS))

enum EKindOfUse : int8;

using String = char*;


using SimOutputStringPtr = String(*)();	                            //sim_output_string

using SimCyclePtr = void(*)();	                                    //sim_cycle
using SimUpdateOutputPtr = void(*)();	                            //sim_update_output
using SimSetOutputPtr = void(*)(int64);	                            //sim_set_output

using SimInitPtr = void*(*)(EKindOfUse, uint64, uint64, uint64);	//sim_init
using SimClosePtr = void(*)();	                                    //sim_close


enum EKindOfUse : int8
{
	KIND_PRE_STARTUP = -2,
	KIND_NOTHING_TO_RUN = -1,
	KIND_LOAD_FILE = 0,
	KIND_NEW_SIMULATION,
	KIND_NEW_APES,
	KIND_START_UP,
	KIND_MEMORY_SETUP,
};

struct FApeSDKDLLWrapper
{
public:
	FApeSDKDLLWrapper(const FString& Path);
	
	~FApeSDKDLLWrapper();
	
	FORCEINLINE void* SimInit(EKindOfUse KindOfUse, uint64 Randomize, uint64 OffScreenSize, uint64 LandBufferSize) const
	{
		void* Result = nullptr;
		
		if(SimInitFunc)
		{
			Result = SimInitFunc(KindOfUse, Randomize, OffScreenSize, LandBufferSize);

			if(SimSetOutputFunc)
			{
				SimSetOutputFunc(1);
			}
		}
		
		return Result;
	}

	FORCEINLINE void SimClose() const
	{
		if(SimCloseFunc)
		{
			SimCloseFunc();
		}
	}

	FORCEINLINE void SimCycle() const
	{
		if(SimCycleFunc)
		{
			SimCycleFunc();
		}

		if(SimUpdateOutputFunc)
		{
			SimUpdateOutputFunc();
		}
	}

	FORCEINLINE FString SimOutputString() const
	{
		if(SimOutputStringFunc)
		{
			if(const String Output = SimOutputStringFunc())
			{
				return FString(Output);
			}
		}

		return FString();
	}
	
private:
	bool TryLoadSDKDLL(const FString& Path, bool bIsEnginePlugin);
	
	void* ApeSDKDLLHandle = nullptr;

	SimInitPtr SimInitFunc = nullptr;
	SimClosePtr SimCloseFunc = nullptr;

	SimCyclePtr SimCycleFunc = nullptr;

	SimSetOutputPtr SimSetOutputFunc = nullptr;
	SimOutputStringPtr SimOutputStringFunc = nullptr;

	SimUpdateOutputPtr SimUpdateOutputFunc = nullptr;
};