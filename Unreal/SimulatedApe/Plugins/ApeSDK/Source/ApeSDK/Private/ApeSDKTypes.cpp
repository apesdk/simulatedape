﻿#include "ApeSDKTypes.h"

FApeSDKDLLWrapper::FApeSDKDLLWrapper(const FString& Path)
{
	const bool bLoadedDLL = TryLoadSDKDLL(Path, false) || TryLoadSDKDLL(Path, true);
	check(bLoadedDLL);

	SimInitFunc = static_cast<SimInitPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("sim_init")));
	SimCloseFunc = static_cast<SimClosePtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("sim_close")));
	SimCycleFunc =  static_cast<SimCyclePtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("sim_cycle")));
	SimOutputStringFunc = static_cast<SimOutputStringPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("sim_output_string")));
	SimSetOutputFunc = static_cast<SimSetOutputPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("sim_set_output")));
	SimUpdateOutputFunc = static_cast<SimUpdateOutputPtr>(FPlatformProcess::GetDllExport(ApeSDKDLLHandle, TEXT("sim_update_output")));
}

FApeSDKDLLWrapper::~FApeSDKDLLWrapper()
{
	if(ApeSDKDLLHandle)
	{
		FPlatformProcess::FreeDllHandle(ApeSDKDLLHandle);
	}
}

bool FApeSDKDLLWrapper::TryLoadSDKDLL(const FString& Path, bool bIsEnginePlugin)
{
	FString FilePath = FPaths::Combine(bIsEnginePlugin ? FPaths::EnginePluginsDir() : FPaths::ProjectPluginsDir(), Path);
	ApeSDKDLLHandle = FPlatformProcess::GetDllHandle(*FilePath);

	return ApeSDKDLLHandle != nullptr;
}