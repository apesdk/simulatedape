// Copyright Epic Games, Inc. All Rights Reserved.

#include "ApeSDK.h"


#define LOCTEXT_NAMESPACE "FApeSDKModule"

void FApeSDKModule::StartupModule()
{
	ApeSDKWrapper = MakeShareable(new FApeSDKDLLWrapper(FPaths::Combine("ApeSDK", "apesdk.dll")));
}

void FApeSDKModule::ShutdownModule()
{
	ApeSDKWrapper.Reset();
}

FApeSDKModule& FApeSDKModule::Get()
{
	return FModuleManager::LoadModuleChecked<FApeSDKModule>(TEXT("ApeSDK"));
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FApeSDKModule, ApeSDK)