// Fill out your copyright notice in the Description page of Project Settings.


#include "SimulatedApeGameModeBase.h"

#include "SimulatedApe/UI/SimulatedApeHUDBase.h"

ASimulatedApeGameModeBase::ASimulatedApeGameModeBase(const FObjectInitializer& Initializer)
	: Super(Initializer)
{
	HUDClass = ASimulatedApeHUDBase::StaticClass();
}