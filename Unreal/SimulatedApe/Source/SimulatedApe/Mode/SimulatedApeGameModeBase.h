// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SimulatedApeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SIMULATEDAPE_API ASimulatedApeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	ASimulatedApeGameModeBase(const FObjectInitializer& Initializer);
};
