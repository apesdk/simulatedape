﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SimulatedApeHUDBase.h"

#include "ApeSDK/ApeSimulationMainWidget.h"
#include "Blueprint/UserWidget.h"


void ASimulatedApeHUDBase::BeginPlay()
{
	Super::BeginPlay();

	MainWidget = CreateWidget<UApeSimulationMainWidget>(GetOwningPlayerController(), WidgetClass);
	MainWidget->AddToViewport();
}

void ASimulatedApeHUDBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	MainWidget->RemoveFromParent();
}