import apesdk
import pygame
import pygame.freetype
import sys
import random
import time
import graphics_aux
from pygame.locals import *
 
class SimulApe:

	def __init__(self):
		self.is_running = True
		self.display = None
		self.size = self.weight, self.height = 1024, 768
		self.map_size = 512
		self.font_size = 24

		rng = random.Random(time.time())
		number = rng.randint(0, sys.maxsize >> 32)	# sys.maxsize is too big for apesdk_init

		apesdk.dll.apesdk_init(number, self.map_size * self.map_size, 0)

		pygame.init()
	
		pygame.freetype.init()

		self.font =  pygame.freetype.SysFont(pygame.font.get_default_font(), self.font_size)

		self.display = pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF, 8)
		self.mini_map = pygame.Surface((self.map_size, self.map_size))
		self.terrain = pygame.Surface((self.map_size, self.map_size))
		
	def __del__(self):
		apesdk.dll.apesdk_close()
		pygame.quit()
		pygame.freetype.quit()

	def process_event(self, event):
		if event.type == pygame.QUIT:
			self.is_running = False

	def run_loop(self):
		apesdk.dll.apesdk_cycle(int(time.time()))

		self.output = apesdk.dll.apesdk_get_output_string()

	def render(self):
		terrain_view = apesdk.dll.apesdk_draw(1, self.map_size, self.map_size)
		#view = apesdk.dll.apesdk_draw(0, self.map_size, self.map_size)

		self.display.fill(0)

		#graphics_aux.copy_view_to_surface(view, self.mini_map, (self.map_size, self.map_size))
		graphics_aux.copy_view_to_surface(terrain_view, self.terrain, (self.map_size, self.map_size))

		self.display.blit(self.terrain, (0, 0))
		self.display.blit(self.mini_map, (512, 0))
		self.font.render_to(self.display, (10, 512), self.output, (255, 255, 255))

		pygame.display.flip()

	def execute(self):
		self.is_running = True

		clock = pygame.time.Clock()
		
		while(self.is_running):
			clock.tick(60)
			
			for event in pygame.event.get():
				self.process_event(event)

			self.run_loop()
			self.render()

if __name__ == "__main__" :
	simulApe = SimulApe()
	simulApe.execute()