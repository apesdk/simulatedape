import pygame

def copy_view_to_surface(view, surface, size):
	
    for i in range(size[1]):
        for j in range(size[0]):
            pixel = view[i * size[0] + j]
            surface.set_at((j, i), get_color_from_byte(pixel))

def get_color_from_byte(num):
    return ((num & 0x0000FF00) >> 8, (num & 0x00FF0000) >> 16, (num & 0xFF000000) >> 24, 255 - (num & 0x000000FF))